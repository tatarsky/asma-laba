.intel_syntax noprefix

SYS_EXIT = 1
SYS_READ = 3
SYS_WRITE = 4

STDIN = 0
STDOUT = 1
STDERR = 2

.data

end_of_line:
.ascii "\n"

buf:
.space 12, 0

str:
.ascii "Введите 12 символов:"

.text

.global _start

_start :

mov edx, 36
mov ecx, offset str
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 12
mov ecx, offset buf
mov ebx, STDIN
mov eax, SYS_READ
int 0x80

mov edx, 1
lea eax, [buf + 3]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80


mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
lea eax, [buf + 6]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80


mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
lea eax, [buf + 7]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
lea eax, [buf + 0]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80


mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
lea eax, [buf + 10]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80


mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov ebx, 0
mov eax, SYS_EXIT
int 0x80
