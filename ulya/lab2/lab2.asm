.intel_syntax noprefix 
SYS_EXIT = 1
SYS_READ = 3
SYS_WRITE = 4

STDIN = 0
STDOUT = 1
STDERR = 2

.data

error_msg:
.ascii "Error reading STDIN\n"

end_of_line:
.ascii "\n"


buf:
.space 4, 0

str:
.ascii "Enter 4 numbers"

.text

.global _start

_start :

mov edx, 15
mov ecx, offset str
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 4
mov ecx, offset buf
mov ebx, STDIN
mov eax, SYS_READ
int 0x80

cmp eax, -1
jz error

// ((D+C)-B)-A-(C+B)





// Считывание A
xor eax, eax
mov al, [buf + 0]
sub al, '0

// Считывание B
xor ebx, ebx
mov bl, [buf + 1]
sub bl, '0


// Считывание D
xor edx, edx
mov dl, [buf + 3]
sub dl, '0


sub dl, al
sub dl, bl
sub dl, bl

add edx, '0
mov [buf], edx



// Вывод результатов
mov edx, 1
mov ecx, offset buf
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80
jmp exit

error:
mov edx, 20
mov ecx, offset error_msg
mov ebx, STDERR
mov eax, SYS_WRITE
int 0x80

exit:
mov ebx, 0
mov eax, SYS_EXIT
int 0x80
