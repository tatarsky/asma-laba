.intel_syntax noprefix

SYS_EXIT = 1
SYS_READ = 3
SYS_WRITE = 4

STDIN = 0
STDOUT = 1
STDERR = 2

.data

end_of_line:
.ascii "\n"

buf:
.space 10, 0

str:
.ascii "Введите 10 символов:"

.text

.global _start

_start :

mov edx, 36
mov ecx, offset str
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 10
mov ecx, offset buf
mov ebx, STDIN
mov eax, SYS_READ
int 0x80

mov edx, 1
lea eax, [buf + 4]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80


mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
lea eax, [buf + 8]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80


mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
lea eax, [buf + 1]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
lea eax, [buf + 1]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80


mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
lea eax, [buf + 6]
mov ecx, eax
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80


mov edx,1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov ebx, 0
mov eax, SYS_EXIT
int 0x80
