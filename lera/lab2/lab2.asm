.intel_syntax noprefix 
SYS_EXIT = 1
SYS_READ = 3
SYS_WRITE = 4

STDIN = 0
STDOUT = 1
STDERR = 2

.data

error_msg:
.ascii "Error reading STDIN\n"

end_of_line:
.ascii "\n"


buf:
.space 4, 0

str:
.ascii "Enter 4 numbers"

.text

.global _start

_start :

mov edx, 15
mov ecx, offset str
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 4
mov ecx, offset buf
mov ebx, STDIN
mov eax, SYS_READ
int 0x80

cmp eax, -1
jz error

// (А+B)-C+((D-B)+C)



// Считывание D
xor edx, edx
mov dl, [buf + 3]
sub dl, '0


// Считывание A
xor eax, eax
mov al, [buf + 0]
sub al, '0

add bl, al
add bl, dl

add ebx, '0
mov [buf], ebx



// Вывод результатов
mov edx, 1
mov ecx, offset buf
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80

mov edx, 1
mov ecx, offset end_of_line
mov ebx, STDOUT
mov eax, SYS_WRITE
int 0x80
jmp exit

error:
mov edx, 20
mov ecx, offset error_msg
mov ebx, STDERR
mov eax, SYS_WRITE
int 0x80

exit:
mov ebx, 0
mov eax, SYS_EXIT
int 0x80
